package br.com.fiap.appassistencia;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONStringer;

import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private EditText edtCodigoFuncionario;
    private Spinner spinnerServico;
    private CheckBox checkBoxFinalizado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.item_chamados){
            Intent intent = new Intent(this, ChamadosActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    private void initViews() {
        edtCodigoFuncionario = (EditText) findViewById(R.id.edtCodigoFuncionario);
        spinnerServico = (Spinner) findViewById(R.id.spinnerServico);
        checkBoxFinalizado = (CheckBox) findViewById(R.id.checkBoxFinalizado);
    }

    public void cadastrar(View v) {

        int codigoFuncionario = Integer.parseInt(edtCodigoFuncionario.getText().toString());
        String descricao = spinnerServico.getSelectedItem().toString();
        boolean finalizado = checkBoxFinalizado.isChecked() ? true : false;

        Toast.makeText(this, "id = " + String.valueOf(codigoFuncionario) +
                "\nDesc = " + descricao +
                "\nFinalizado = " + String.valueOf(finalizado), Toast.LENGTH_SHORT).show();

        BackgroundTask backgroundTask = new BackgroundTask();

        backgroundTask.execute( String.valueOf(codigoFuncionario),
                                descricao,
                                String.valueOf(finalizado) );
    }

    private class BackgroundTask extends AsyncTask<String,Void,Integer> {

        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(MainActivity.this,
                    "Aguarde","Enviando dados..");
        }

        @Override
        protected void onPostExecute(Integer status) {
            progressDialog.dismiss();
            if (status == 201){
                Toast.makeText(MainActivity.this,"Sucesso!",
                        Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(MainActivity.this,"Erro!",
                        Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected Integer doInBackground(String... params) {
            try {
                URL url = new URL(
                        "https://assistenciaapi.herokuapp.com/rest/chamado");
                HttpURLConnection connection =
                        (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type","application/json");

                JSONStringer json = new JSONStringer();
                json.object();
                json.key("codigoFuncionario").value(params[0]);
                json.key("descricao").value(params[1]);
                json.key("finalizado").value(params[2]);
                json.endObject();

                OutputStreamWriter writer =
                        new OutputStreamWriter(connection.getOutputStream());
                writer.write(json.toString());
                writer.close();

                return connection.getResponseCode();

            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }
    }

}
