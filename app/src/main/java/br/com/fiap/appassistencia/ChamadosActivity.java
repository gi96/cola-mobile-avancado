package br.com.fiap.appassistencia;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import br.com.fiap.appassistencia.model.Chamado;

public class ChamadosActivity extends AppCompatActivity {

    private EditText edtCodigoFuncionario;
    private ListView listView;
    private ArrayAdapter<Chamado> adapter;
    private ArrayList<Chamado> chamados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chamados);

        initViews();
    }

    private void initViews() {
        edtCodigoFuncionario = (EditText) findViewById(R.id.edtCodigoFuncionario);
        listView = (ListView) findViewById(R.id.listView);
    }

    public void pesquisar(View view) {
        int codigo = Integer.parseInt(edtCodigoFuncionario.getText().toString());
        BackgroundTask backgroundTask = new BackgroundTask();
        backgroundTask.execute(codigo);
    }

    private class BackgroundTask extends AsyncTask<Integer, Void, String> {

        private ProgressDialog progress;

        @Override
        protected void onPreExecute() {
            progress = ProgressDialog.show(ChamadosActivity.this, "Aguarde","Chamando o servidor");
        }

        @Override
        protected void onPostExecute(String s) {
            progress.dismiss();
            if (s != null){
                try {
                    JSONArray jsonArray = new JSONArray(s);

                    chamados = new ArrayList<>();

                    for (int i=0; i < jsonArray.length(); i++) {
                        JSONObject item = (JSONObject) jsonArray.get(i);
                        int codigo = item.getInt("codigo");
                        int codigoFuncionario = item.getInt("codigoFuncionario");
                        String data = item.getString("data");
                        boolean finalizado = item.getBoolean("finalizado");
                        String descricao = item.getString("descricao");

                        Chamado chamado =
                                new Chamado(codigo, codigoFuncionario, data, descricao, finalizado);

                        chamados.add(chamado);
                    }

                    adapter = new ArrayAdapter<>(ChamadosActivity.this, android.R.layout.simple_list_item_1, chamados);
                    listView.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(ChamadosActivity.this, "Erro", Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected String doInBackground(Integer... params) {

            try {
                URL url = new URL("https://assistenciaapi.herokuapp.com/rest/chamado/funcionario/" + params[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Accept","application/json");

                if (connection.getResponseCode() == 200){
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(connection.getInputStream()));

                    StringBuilder retorno = new StringBuilder();
                    String linha;
                    while ((linha = reader.readLine()) != null){
                        retorno.append(linha);
                    }
                    connection.disconnect();
                    return retorno.toString();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

    }
}
