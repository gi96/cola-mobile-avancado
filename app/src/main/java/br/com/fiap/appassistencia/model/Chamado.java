package br.com.fiap.appassistencia.model;

/**
 * Created by joseleonardocangelli on 03/09/17.
 */

public class Chamado {

    private int codigo;
    private int codigoFuncionario;
    private String data;
    private String descricao;
    private boolean finalizado;

    public Chamado() {

    }

    public Chamado(int codigo, int codigoFuncionario, String data, String descricao, boolean finalizado) {
        this.codigo = codigo;
        this.codigoFuncionario = codigoFuncionario;
        this.data = data;
        this.descricao = descricao;
        this.finalizado = finalizado;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getCodigoFuncionario() {
        return codigoFuncionario;
    }

    public void setCodigoFuncionario(int codigoFuncionario) {
        this.codigoFuncionario = codigoFuncionario;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public boolean isFinalizado() {
        return finalizado;
    }

    public void setFinalizado(boolean finalizado) {
        this.finalizado = finalizado;
    }

    @Override
    public String toString() {
        return codigo + " - " +
                descricao + " - " +
                data + " - " +
                "Finalizado: " + finalizado;
    }
}

